import sys
import math
from collections import Counter

def dbg(msg):
    print(msg, file=sys.stderr)

class SkynetNetwork:
    def __init__(self, nodes):
        self.links = []
        for i in range(nodes):
            self.links.append([])
        self.skynet = None
        self.gateways = None

    def set_skynet(self, pos):
        self.skynet = pos

    def set_gateways(self, gateways):
        self.gateways = gateways

    def add_connection(self, a, b):
        self.links[a].append(b)
        self.links[b].append(a)

    def remove_connection(self, a, b):
        self.links[a].remove(b)
        self.links[b].remove(a)

    def degree(self, v):
        return len(set(self.gateways) & set(self.links[v]))

    def get_skynet_dist(self, b):
        reached = set()
        in_range = [self.skynet]
        targets = []
        dist = 0
        while len(in_range)>0:
            in_next_range = []
            # Following iterates over all elements in range of dist
            for v in in_range:
                if v in self.gateways or v in reached:
                    continue
                if v == b:
                    return (dist, b)
                if self.degree(v)>0:
                    for w in set(self.links[v]):
                        in_range.append(w)
                else:
                    for w in set(self.links[v]):
                        in_next_range.append(w)
                reached.add(v)
            in_range = in_next_range
            dist += 1
        return (dist, b)

    def find_route(self):
        # Check neighbours
        for v in self.links[self.skynet]:
            if v in self.gateways:
                return (self.skynet, v)

        #  second-degrees
        cnt = Counter()
        for b in self.gateways:
            cnt.update(self.links[b])
        doubles = list(map(lambda x: x[0], filter(lambda x: x[1]==2, cnt.most_common())))
        closest = sorted(map(self.get_skynet_dist, doubles))
        if len(closest)>0:
            target = closest[0][1]
            target_gw = list(set(self.links[target]) & (set(self.gateways)))[0]
            return (target, target_gw)
        else:
            for b in self.gateways:
                if len(self.links[b])>0:
                    return (self.links[b][0], b)

# n: the total number of nodes in the level, including the gateways
# l: the number of links
# e: the number of exit gateways
n, l, e = [int(i) for i in input().split()]

network = SkynetNetwork(n)
for i in range(l):
    # n1: N1 and N2 defines a link between these nodes
    n1, n2 = [int(j) for j in input().split()]
    network.add_connection(n1, n2)

gateways = []
for i in range(e):
    gateways.append(int(input()))  # the index of a gateway node
network.set_gateways(gateways)

# game loop
while True:
    si = int(input())  # The index of the node on which the Skynet agent is positioned this turn
    network.set_skynet(si)
    step = network.find_route()
    print(step[0], step[1])
    network.remove_connection(*step)
