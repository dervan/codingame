import sys
import math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.
def dbg(msg):
    print(msg, file=sys.stderr)
class Graph:

    def __init__(self, nodes):
        self.links = []
        for i in range(nodes):
            self.links.append([])

    def add_connection(self, a, b):
        dbg(a)
        dbg(b)
        dbg("=")
        self.links[a].append(b)
        self.links[b].append(a)

    def remove_connection(self, a, b):
        self.links[a].remove(b)
        self.links[b].remove(a)

    def find_route(self, a, bs):
        for b in bs:
            if b in self.links[a]:
                return b
        return self.links[a][0]

# n: the total number of nodes in the level, including the gateways
# l: the number of links
# e: the number of exit gateways
n, l, e = [int(i) for i in input().split()]

network = Graph(n)
for i in range(l):
    # n1: N1 and N2 defines a link between these nodes
    n1, n2 = [int(j) for j in input().split()]
    network.add_connection(n1, n2)

gateways = []
for i in range(e):
    gateways.append(int(input()))  # the index of a gateway node

# game loop
while True:
    si = int(input())  # The index of the node on which the Skynet agent is positioned this turn

    step = network.find_route(si, gateways)
    print(si, step)

    # Write an action using print
    # To debug: print("Debug messages...", file=sys.stderr)
