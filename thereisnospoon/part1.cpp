#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

template <class T>
void print(T ** tab, int rows, int cols){
  for(int i = 0; i < rows; i++){
    for(int j = 0; j < cols; j++){
      cerr << tab[i][j] << " ";
    }
    cerr << endl;
  }
  cerr << "---------" << endl;
}

int main()
{
    int width; // the number of cells on the X axis
    cin >> width; cin.ignore();
    int height; // the number of cells on the Y axis
    cin >> height; cin.ignore();
    char nodes[height][width];
    for (int i = 0; i < height; i++) {
        string line; // width characters, each either 0 or .
        getline(cin, line);
        for(int j = 0; j < width; j++){
            nodes[i][j] = line[j];
        }
    }
    int *right[height];
    int *bottom[height];
    for(int i=0; i < height; i++){
      right[i] = new int[width];
      bottom[i] = new int[width];
      for(int j=0; j < width; j++){
        right[i][j]={-1};
        bottom[i][j]={-1};
      }
    }
    for (int i = height-1; i >=0; i--) {
        for (int j = width-1; j >=0; j--) {
            if(j<width-1){
                if(nodes[i][j+1]=='0'){
                    right[i][j]=j+1;
                }else{
                    right[i][j]=right[i][j+1];
                }
            }
            if(i<height-1){
                if(nodes[i+1][j]=='0'){
                    bottom[i][j]=i+1;
                }else{
                    bottom[i][j]=bottom[i+1][j];
                }
            }
        }
    }
    print(right, height, width);
    print(bottom, height, width);
    // Three coordinates: a node, its right neighbor, its bottom neighbor
    for (int i = 0; i < height; i++) {
        for(int j = 0; j < width; j++){
            if( nodes[i][j] != '0' )
                continue;
            cout << j << " " << i << " ";
            
            if(right[i][j] != -1)
                cout << right[i][j] << " " << i;
            else 
                cout << "-1 -1";
            
            cout << " ";
            
            if(bottom[i][j] != -1)
                cout << j << " " << bottom[i][j];
            else 
                cout << "-1 -1";
            
            cout << endl;
        }
    }
    
}
