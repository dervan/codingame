import java.util.*
import kotlin.collections.HashMap
import kotlin.math.abs

fun log(s: String) = System.err.println(s)

//-1 for NONE, 2 for RADAR, 3 for TRAP, 4 for ORE)
fun toItem(number: Int): Item = when (number) {
    -1 -> Item.None
    2 -> Item.Radar
    3 -> Item.Trap
    4 -> Item.Ore
    else -> Item.Unknown
}

enum class Item {
    None, Radar, Trap, Ore, Unknown
}

open class Entity(val id: Int, val x: Int, val y: Int) {
    fun dist(field: Pair<Int, Int>): Int {
        return abs(field.first - x) + abs(field.second - y)
    }
}

class Robot(id: Int, x: Int, y: Int, var item: Item) : Entity(id, x, y) {
    var target: Pair<Int, Int>? = null
    var requstedItem: Item? = null
    fun free(): Boolean = target == null

    fun getOrder(): String {
        if (Item.Ore == item) {
            target = Pair(0, y)
        }
        if (requstedItem != null) {
            return "REQUEST " + when (requstedItem) {
                Item.Radar -> "RADAR"
                Item.Trap -> "TRAP"
                else -> throw java.lang.IllegalArgumentException("Cannot request $requstedItem")
            }
        }
        return if (target != null) {
            if (Pair(x, y) == target) {
                this.target = null
                "DIG $x $y"
            } else {
                "MOVE ${target!!.first} ${target!!.second}"
            }
        } else {
            "WAIT"
        }
    }

    fun request(item: Item) {
        requstedItem = item
    }
}


class RobotMemory() {
    private val position = HashMap<Int, Pair<Int, Int>>()
    private val baseRadarPose = Pair<Int, Int>(1, 5)
    var shift = Pair<Int, Int>(0, 0)

    fun save(robot: Robot) {
        val target = robot.target
        if (target != null) {
            position[robot.id] = target
        } else {
            position.remove(robot.id)
        }
    }

    fun refreshMemory(robot: Robot) {
        if (position[robot.id] != null) {
            robot.target = position[robot.id]
        }
    }

    private fun convert(radarMod: Pair<Int, Int>): Pair<Int, Int> {
        val x = baseRadarPose.first + radarMod.first * 4
        val y = baseRadarPose.second + radarMod.second * 8 - (radarMod.first % 2) * 4
        return Pair<Int, Int>(x, y)
    }

    fun nextPose(limit: Pair<Int, Int>): Pair<Int, Int> {
        val pose = convert(shift)
        var nextShift = Pair(shift.first, shift.second + 1)
        val nextY = convert(nextShift)
        log("nextY $nextY $nextShift")
        if (nextY.first < limit.first && nextY.second < limit.second) {
            shift = nextShift
        } else {
            nextShift = Pair(shift.first + 1, 0)
            val nextX = convert(nextShift)
            log("nextX $nextX $nextShift")
            if (nextX.first < limit.first && nextX.second < limit.second) {
                shift = nextShift
            } else {
                log("Random :(")
                val random = Random()
                return Pair(random.nextInt(limit.first), random.nextInt(limit.second))
            }
        }
        return pose
    }
}

/**
 * Deliver more ore to hq (left side of the map) than your opponent. Use radars to find ore but beware of traps!
 **/
class Game(val width: Int, val height: Int, val memory: RobotMemory) {
    var radarCooldown: Int = 0
    private val holes = HashMap<Pair<Int, Int>, Boolean>()
    private val ore = HashMap<Pair<Int, Int>, Int>()
    val robots = ArrayList<Robot>()

    fun setHole(x: Int, y: Int) {
        holes[Pair(x, y)] = true
    }

    fun setOre(x: Int, y: Int, oreAmount: Int) {
        ore[Pair(x, y)] = oreAmount
    }

    fun updateEntity(entity: Entity) {
        if (entity is Robot) {
            robots.add(entity)
        }
    }

    fun process() {
        for (robot in robots) {
            if (!robot.free() && robot.item == Item.None) {
                robot.target = null
            }
        }
        for (oreField in ore.entries) {
            if (oreField.value > 0) {
                val robot = closestFreeRobot(oreField.key)
                robot?.target = oreField.key
            }
        }
        val free = robots.filter { it.free() }
        if (free.isNotEmpty()) {
            val robot = free[0]
            if (radarCooldown == 0) {
                robot.request(Item.Radar)
                val nextPose = memory.nextPose(Pair(width, height))
                log("next selected pose: $nextPose")
                robot.target = nextPose
            }
        }
    }

    private fun closestFreeRobot(field: Pair<Int, Int>): Robot? {
        var closestDist = Integer.MAX_VALUE
        var selectedRobot: Robot? = null
        for (robot in robots) {
            if (robot.free()) {
                val dist = robot.dist(field)
                if (dist < closestDist) {
                    closestDist = dist
                    selectedRobot = robot
                }
            }
        }
        return selectedRobot
    }
}


fun main(args: Array<String>) {
    val input = Scanner(System.`in`)
    val width = input.nextInt()
    val height = input.nextInt() // size of the map

    val memory = RobotMemory()
    // game loop
    while (true) {
        val field = Game(width, height, memory)
        val myScore = input.nextInt() // Amount of ore delivered
        val opponentScore = input.nextInt()
        for (y in 0 until height) {
            for (x in 0 until width) {
                val ore = input.next() // amount of ore or "?" if unknown
                if (ore != "?") {
                    val oreAmount = Integer.parseInt(ore)
                    field.setOre(x, y, oreAmount)
                }

                val hole = input.nextInt() // 1 if cell has a hole
                if (hole == 1) {
                    field.setHole(x, y)
                }
            }
        }
        val orders = ArrayList<String>()
        val entityCount = input.nextInt() // number of entities visible to you
        val radarCooldown = input.nextInt() // turns left until a new radar can be requested
        field.radarCooldown = radarCooldown
        val trapCooldown = input.nextInt() // turns left until a new trap can be requested
        for (i in 0 until entityCount) {
            val id = input.nextInt() // unique id of the entity
            val type = input.nextInt() // 0 for your robot, 1 for other robot, 2 for radar, 3 for trap
            val x = input.nextInt()
            val y = input.nextInt() // position of the entity
            val item =
                input.nextInt() // if this entity is a robot, the item it is carrying (-1 for NONE, 2 for RADAR, 3 for TRAP, 4 for ORE)
            val entity = when (type) {
                0 -> Robot(id, x, y, toItem(item)).also { memory.refreshMemory(it) }
                1, 2, 3 -> Entity(id, x, y)
                else -> throw IllegalArgumentException("Illegal entity type")
            }
            field.updateEntity(entity)
        }

        field.process()

        for (robot in field.robots) {
            memory.save(robot)
            println(robot.getOrder())
        }
    }
}


