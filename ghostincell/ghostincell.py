import random
import sys
import math
def dbg(msg):
    print(msg, file=sys.stderr)
MINE = 1
OPPONENT = -1
NEUTRAL = 0

class Factory:
    def __init__(self, fid, owner, cyborgs, production):
        self.id = fid
        self.owner = owner
        self.cyborgs = cyborgs
        self.production = production
        self.troopsTo = []
    def __repr__(self):
        return str(self)
    def __str__(self):
        return "F{} {} {} +{}".format(self.id, self.owner, self.cyborgs, self.production)

    def value(self):
        return self.production * 3 - self.cyborgs

    def get_source(self, sources, distances):
        s = list(filter(lambda x: x.cyborgs>self.cyborgs, sources))
        if len(s) == 0:
            s = list(filter(lambda x: x.cyborgs>0, sources))
        s = sorted(s, key=lambda x: distances[(self.id, x.id)])
        dbg(s)
        if len(s)>0:
            return s[0]
        else:
            return None

class Match:
    def __init__(self):
        self.src = 0
        self.dst = 0
        self.cnt = 0
    def __str__(self):
        return "M{}-{}->{}".format(self.src, self.cnt, self.dst)
    def __repr__(self):
        return str(self)

def not_mine(factories):
    return list(filter(lambda f: f.owner != MINE, factories))

def mine(factories):
    return list(filter(lambda f: f.owner == MINE, factories))

def match(to_conq, sources, distances):
    matches = []
    for target in to_conq:
        m = Match()
        source = target.get_source(sources, distances)
        if source is None or source.cyborgs<1:
            continue
        m.src = source
        m.dst = target
        matches.append(m)
    mms = []
    for m in matches:
        if m.dst.owner != 0:
            delta =  m.dst.production * distances[(m.src.id, m.dst.id)]
        else:
            delta = 0
        if m.src.cyborgs > m.dst.cyborgs + delta:
            mm = Match()
            mm.src = m.src.id
            mm.dst= m.dst.id
            mm.cnt = max([m.dst.cyborgs+delta+1, m.src.cyborgs//2])
            m.src.cyborgs -= mm.cnt
            mms.append(mm)
    '''for m in matches:
        if m.src.cyborgs <= m.dst.cyborgs:
            mm = Match()
            mm.src = m.src.id
            mm.dst= m.dst.id
            mm.cnt = random.randint(1, max(1, m.src.cyborgs))
            m.src.cyborgs -= mm.cnt
            mms.append(mm)
            '''
    return mms

def all_outs(distances, fact):
    keys = filter(lambda p: p[0] == fact, distances.keys())
    return {key[1]: distances[key] for key in keys}

def fill(distances, fc):
    for i in range(fc):
        visited = {i: 0}
        while len(visited)<fc:
            for v in visited.copy():
                outs = all_outs(distances, v)
                for o in outs:
                    if visited[v]+outs[o] < visited.get(o, 40000):
                        visited[o] = visited[v]+outs[o]
        dbg(str(i)+str(visited))


'''
MAIN PROGRAM
'''
factory_count = int(input())  # the number of factories
link_count = int(input())  # the number of links between factories
distances = {}
for i in range(link_count):
    f1, f2, distance = [int(j) for j in input().split()]
    distances[(f1, f2)] = distance
    distances[(f2, f1)] = distance

fill(distances, factory_count)

def increase(factories):
    for f in factories:
        if f.production == 3:
            continue
        if f.cyborgs>11:
            print("INC " + str(f.id), end=";")
            f.cyborgs -= 10
        elif f.production > 0:
            f.cyborgs = 0

# game loop
while True:
    factories = []
    entity_count = int(input())  # the number of entities (e.g. factories and troops)
    for i in range(entity_count):
        entity_id, entity_type, arg_1, arg_2, arg_3, arg_4, arg_5 = input().split()
        entity_id = int(entity_id)
        arg_1 = int(arg_1)
        arg_2 = int(arg_2)
        arg_3 = int(arg_3)
        arg_4 = int(arg_4)
        arg_5 = int(arg_5)
        if(entity_type == "FACTORY"):
            factories.append(Factory(entity_id, arg_1, arg_2, arg_3))

    to_conq = sorted(not_mine(factories), key=lambda f: f.value(), reverse=True)
    sources = sorted(list(filter(lambda x: x.cyborgs>0, mine(factories))), key=lambda f:f.cyborgs, reverse=True)
    avgc = 0
    if len(sources)>0:
        avgc = sum(map(lambda x: x.cyborgs, mine(factories)))/len(mine(factories))
    if avgc < 10 or len(sources)<=2:
        to_conq = list(filter(lambda x: x.production>0, to_conq))
    if len(sources)>1:
        increase(sources)
    matching = match(to_conq, sources, distances)
    dbg(matching)
    if len(matching)>0:
        for m in matching:
            print("MOVE {} {} {}".format(m.src, m.dst, m.cnt), end=";")
    print("WAIT")
