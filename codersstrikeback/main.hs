import System.IO
import Control.Monad
import Control.Monad.State  

getThrust :: Checkpoint -> Int
getThrust checkpoint =
    let ang =  (angle checkpoint) in
    if abs ang > 90
        then 10
        else if (dist checkpoint) < 1000 
            then 90 - (quot ((ang*ang)*100) (90*90))
            else 100

main :: IO ()
main = do
    hSetBuffering stdout NoBuffering -- DO NOT REMOVE
    
    -- Auto-generated code below aims at helping you parse
    -- the standard input according to the problem statement.
    
     runState loop (GameState False False)
    
data Checkpoint = Checkpoint {
    xPos :: Int,
    yPos :: Int,
    dist :: Int,
    angle :: Int }
    
data GameState = GameState {
    usedBoost :: Bool,
    usedShield :: Bool
}

boost :: State GameState ()
boost = state $ \s -> ((),  GameState { usedBoost = True, usedShield = usedShield s })

shield :: State GameState ()
shield = state $ \s -> ((),  GameState { usedBoost = usedBoost s, usedShield = True })

canBoost :: State GameState Bool
canBoost = state $ \s -> (not $ usedBoost s, s)

canShield :: State GameState Bool
canShield = state $ \s -> (not $ usedShield s, s)


loop :: IO (State GameState ())
loop = do
    input_line <- getLine
    let input = words input_line
    let x = read (input!!0) :: Int
    let y = read (input!!1) :: Int
    let checkpoint = Checkpoint (read (input!!2)) (read (input!!3)) (read (input!!4)) (read (input!!5))
    input_line <- getLine
    let input = words input_line
    let opponentx = read (input!!0) :: Int
    let opponenty = read (input!!1) :: Int
    let thrust = getThrust checkpoint
    -- hPutStrLn stderr "Debug messages..."
    -- You have to output the target position
    -- followed by the power (0 <= thrust <= 100)
    -- i.e.: "x y thrust"
    
    putStrLn (show (xPos checkpoint) ++ " " ++ show (yPos checkpoint) ++ " " ++ show thrust )
    
    loop
